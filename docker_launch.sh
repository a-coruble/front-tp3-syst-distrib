print_info() {
    local message="$@"
    echo "\e[1;32m${message}\e[0m"
}

help () {
    echo
    echo "docker_launch [cmd]"
    echo
    echo "-h|--help    Affiche de l'aide pour lancer le programme"
    echo "create       Créer le conteneur docker pour la première fois"
    echo "stop         Stop le conteneur docker"
    echo "start        Lance le conteneur docker si existant"
    echo "delete       Lance le conteneur docker si existant"
}

create() {
    docker build -t sys_dys_tp3_marwan_arthur .
    docker ps -l
    docker run -d -it -p 3000:3000 --name=philosophers sys_dys_tp3_marwan_arthur
    echo
    print_info "Veuillez ouvrir votre navigateur à l'adresse http://localhost:3000"
}

stop () {
    print_info "Le conteneur est en train d'être arrété, veuillez patienter..."
    docker stop philosophers
    print_info "Le conteneur a été stoppé"
}

start() {
    docker start philosophers
    echo
    print_info "Veuillez ouvrir votre navigateur à l'adresse http://localhost:3000"
}

delete() {
    docker rm philosophers
    echo
    print_info "Le conteneur à été supprimé"
}

if [ $# -ne 1 ]; then
    help
else
    arg="$1"
    case $arg in 
        -h | --help)
            help
        ;;
        create)
            create
            ;;
        stop)
            stop
            ;;
        start)
            start
            ;;
        delete)
            delete
            ;;
        *)
            echo "Argument inconnu : ${arg}"
            help
            ;;
    esac
fi