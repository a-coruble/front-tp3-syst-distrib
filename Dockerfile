FROM kkarczmarczyk/node-yarn

MAINTAINER marwan.liani@epitech.eu

COPY . /home/docker

RUN chmod 744 /home/docker/launch.sh

RUN ls /home/docker

EXPOSE 3000

ENTRYPOINT /home/docker/launch.sh

WORKDIR /home/docker/