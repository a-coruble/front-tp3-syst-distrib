import React from 'react'
import {Box, Row, Col} from 'jsxstyle'
import {Dimmer, Loader, Message, Segment, Header} from 'semantic-ui-react'

// Importation des composants crées au cours du TP
import {AppContext} from './context'
import {filterList} from './utils'
import PhilosopherCard from './components/PhilosopherCard'
import PhilosopherList from './components/PhilosopherList'
import SearchBar from './components/SearchBar'

// Ce composant est le composant principal de l'application, c'est lui qui s'occuppe de déterminer
// quels autres composants doit être affiché en fonction des données disponibles et des clics
export default class Philosopher extends React.Component {
  render() {
    const {
      isFetchingPhilosophers,
      errorFetchingPhilosophers,
      philosophers,
      currentPhilosopher,
      setCurrentPhilosopher,
    } = this.context
    return (
      <Box>
        <SearchBar />
        {errorFetchingPhilosophers && (
          <Message
            error
            header="Error fetching philosopher"
            content="An error has occured while fetching the data, please reload the page"
            attached="top"
            floating
          />
        )}
        {isFetchingPhilosophers ? (
          <Dimmer.Dimmable>
            <Box height="90vh">
              <Dimmer active>
                <Loader>Wait a minute, we are fetching the data for you</Loader>
              </Dimmer>
            </Box>
          </Dimmer.Dimmable>
        ) : currentPhilosopher ? (
          <Box height="90vh" justifyContent="space-evenly" display="flex" flexWrap="wrap">
            <Col justifyContent="center" alignSelf="center">
              <Segment raised textAlign="center">
                <Header>{currentPhilosopher.name} has been influenced by :</Header>
              </Segment>
              <Row justifyContent="center" alignSelf="center" overflowY="hidden" maxHeight="70vh" height="70vh">
                <PhilosopherList
                  philosophers={filterList(philosophers, currentPhilosopher, 'influencedBy')}
                  setCurrentPhilosopher={setCurrentPhilosopher}
                />
              </Row>
            </Col>
            <Row justifyContent="center" alignSelf="center" maxHeight="70vh" height="70vh">
              <PhilosopherCard philosopher={currentPhilosopher} />
            </Row>

            <Col justifyContent="center" alignSelf="center">
              <Segment raised textAlign="center">
                <Header>{currentPhilosopher.name} has influenced :</Header>
              </Segment>
              <Row justifyContent="center" alignSelf="center" overflowY="hidden" maxHeight="70vh" height="70vh">
                <PhilosopherList
                  philosophers={filterList(philosophers, currentPhilosopher, 'influenced')}
                  setCurrentPhilosopher={setCurrentPhilosopher}
                />
              </Row>
            </Col>
          </Box>
        ) : (
          <Box height="90vh" justifyContent="center" display="flex">
            <Col justifyContent="center" alignSelf="center">
              <Segment raised textAlign="center">
                <Header>List of all philosophers :</Header>
              </Segment>
              <Row justifyContent="center" alignSelf="center" overflowY="hidden" maxHeight="80vh" height="80vh">
                <PhilosopherList philosophers={philosophers} setCurrentPhilosopher={setCurrentPhilosopher} />
              </Row>
            </Col>
          </Box>
        )}
      </Box>
    )
  }
}

Philosopher.contextType = AppContext
