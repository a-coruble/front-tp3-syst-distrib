import React from 'react'
import {jsonFetch} from 'easyfetch'

import {sortObject} from './utils'

// Ceci est l'état initial des données de l'application.
const defaultAppContext = {
  philosophers: null,
  currentPhilosopher: null,
  isFetchingPhilosophers: true,
  errorFetchingPhilosophers: false,
  isFetchingCurrentPhilosopher: false,
  errorFetchingCurrentPhilosopher: false,
}

// On crée un context, un élément qui va permettre de rendre disponible nos données aux 
// composants que l'on souhaite directement sans devoir les passer depuis le composant
// le plus haut dans l'arbre de rendu.
export const AppContext = React.createContext(defaultAppContext)


// Ce composant est celui qui va permettre d'effectuer toutes les requêtes de l'API
// et de gérer l'état global de notre application (quel philosophe est sélectionné, y'a-t-il eu des erreurs lors des requêtes)
export default class AppContextProvider extends React.Component {
  constructor(props) {
    super(props)
    this.state = defaultAppContext
  }

  componentDidMount() {
    this.fetchAllPhilosophers()
  }

  // Cette fonction asynchrone nous permet de récupérer la liste de
  // tous les philosophes et de la stocker dans le state de notre application.
  // On récupère leurs noms, leur url de ressource pour les requêtes futures et les images correspondantes
  async fetchAllPhilosophers() {
    const query = encodeURIComponent(
      // eslint-disable-next-line
      'SELECT ?url ?label ?imageURL WHERE {\
        ?url a <http://dbpedia.org/ontology/Philosopher>; \
        rdfs:label ?label. \
        OPTIONAL { ?url dbo:thumbnail ?imageURLValue }. \
        \
        BIND("" as ?defaultValue) \
        BIND(COALESCE(?imageURLValue, ?defaultValue) AS ?imageURL) \
        \
        FILTER langMatches(lang(?label), "en")\
      }'
    )

    await jsonFetch(`http://dbpedia.org/sparql/?query=${query}`)
      .get()
      .then(async data => {
        const philosophers = {}
        const results = data.results.bindings
        for (const philosopher of results) {
          philosophers[philosopher.url.value] = {
            url: philosopher.url.value,
            name: philosopher.label.value,
            imageURL: philosopher.imageURL.value,
          }
        }
        this.setState({
          philosophers: sortObject(philosophers),
          isFetchingPhilosophers: false,
          errorFetchingPhilosophers: false,
        })
      })
      .catch(() => this.setState({isFetchingPhilosophers: false, errorFetchingPhilosophers: true}))
  }

  // Cette fonction va récupérer tout les détails d'un philosophe tels que :
  //  - Nom + Prénom
  //  - Dates de naissance & mort
  //  - Description
  //  - Liste des influenceurs
  //  - Liste des influencés
  //  - Image
  async fetchPhilosopher(url = '') {
    this.setState({isFetchingCurrentPhilosopher: true, errorFetchingCurrentPhilosopher: false})
    const query = encodeURIComponent(
      // eslint-disable-next-line
      `SELECT DISTINCT ?name ?birthDate ?deathDate ?description ?imageURL \
      (GROUP_CONCAT(DISTINCT ?influencedList; separator=", ") AS ?influenced) \
      (GROUP_CONCAT(DISTINCT ?influencedByList; separator=", ") AS ?influencedBy) \
      WHERE { \
            OPTIONAL { <${url}> rdfs:label ?nameValue } . \
            OPTIONAL { <${url}> dbo:birthDate ?birthDateValue }. \
            OPTIONAL { <${url}> dbo:deathDate ?deathDateValue }. \
            OPTIONAL { <${url}> dbo:abstract ?descriptionValue }. \
            OPTIONAL { <${url}> dbo:influenced ?influencedListValue }. \
            OPTIONAL { <${url}> dbo:influencedBy ?influencedByListValue }. \
            OPTIONAL { <${url}> dbo:thumbnail ?imageURLValue }. \
            \
            BIND("" as ?defaultValue) \
            \
            BIND(COALESCE(?nameValue, ?defaultValue) AS ?name) \
            BIND(COALESCE(?birthDateValue, ?defaultValue) AS ?birthDate) \
            BIND(COALESCE(?deathDateValue, ?defaultValue) AS ?deathDate) \
            BIND(COALESCE(?descriptionValue, ?defaultValue) AS ?description) \
            BIND(COALESCE(?influencedListValue, ?defaultValue) AS ?influencedList) \
            BIND(COALESCE(?influencedByListValue, ?defaultValue) AS ?influencedByList) \
            BIND(COALESCE(?imageURLValue, ?defaultValue) AS ?imageURL) \
            \
            FILTER langMatches(lang(?description), "en") \
            FILTER langMatches(lang(?name), "en") \
      } \
      GROUP BY ?name ?birthDate ?deathDate ?description ?imageURL LIMIT 1`
    )

    return jsonFetch(`http://dbpedia.org/sparql/?query=${query}`)
      .get()
      .then(data => {
        if (data.results.bindings.length > 0) {
          const result = data.results.bindings[0]
          const philosopher = {
            name: result.name.value,
            birthDate: result.birthDate.value,
            deathDate: result.deathDate.value,
            description: result.description.value,
            influenced: result.influenced.value.split(', '),
            influencedBy: result.influencedBy.value.split(', '),
            imageURL: result.imageURL.value,
          }
          this.setState({isFetchingCurrentPhilosopher: false, errorFetchingCurrentPhilosopher: false})
          return philosopher
        }
        this.setState({isFetchingCurrentPhilosopher: false, errorFetchingCurrentPhilosopher: true})
        return null
      })
      .catch(() => this.setState({isFetchingCurrentPhilosopher: false, errorFetchingCurrentPhilosopher: true}))
  }

  // Permet de définir le philosophe pour lequel on veut des détails
  setCurrentPhilosopher = async url => {
    const infos = await this.fetchPhilosopher(url)
    this.setState({currentPhilosopher: {...infos}})
  }

  // Permet de vider le philosophe en cours afin de revenir à la liste complète
  resetCurrentPhilosopher = () => {
    this.setState({currentPhilosopher: null, errorFetchingCurrentPhilosopher: false})
  }

  // Cette fonction va permettre d'exposer l'état des données à tout les composants enfants?
  render() {
    return (
      <AppContext.Provider
        value={{
          ...this.state,
          fetchAllPhilosophers: this.fetchAllPhilosophers,
          setCurrentPhilosopher: this.setCurrentPhilosopher,
          resetCurrentPhilosopher: this.resetCurrentPhilosopher,
        }}
      >
        {this.props.children}
      </AppContext.Provider>
    )
  }
}
