// Fonction utilitaire qui permet de trier un objet Javascript selon la valeur de ses clés
export const sortObject = object => {
  const ordered = {}
  Object.keys(object)
    .sort()
    .forEach(key => {
      ordered[key] = object[key]
    })
  return ordered
}

// Fonction utilitaire qui permet de filtrer un objet Javascript {clé: valeur} par rapport
// par rapport à une valeur
export const filterList = (philosophersList, philosopher, propertyName) => {
  const filtered = Object.keys(philosophersList)
    .filter(key => philosopher[propertyName].includes(key))
    .reduce((obj, key) => {
      obj[key] = philosophersList[key]
      return obj
    }, {})
  return sortObject(filtered)
}
