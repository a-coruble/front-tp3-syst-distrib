import React from 'react'
import {Menu, Search, Button} from 'semantic-ui-react'
import _ from 'lodash'

import {AppContext} from '../context'

// Ce composant est la barre de menu qui se trouve en haut de la page
export default class SearchBar extends React.Component {
  // Cette fonction va initialiser l'état du composant
  componentWillMount() {
    this.resetComponent()
  }

  // Cette fonction permet de réinitialiser l'état du composant à son état initial
  resetComponent = () => {
    this.setState({
      isLoading: false,
      results: [],
      value: '',
    })
  }

  // Cette fonction effectue la recherche lorsqu'on saisit une entrée
  // dans la barre de recherche. On utilise des TimeOut afin de s'assurer de ne pas lancer
  // la recherche tant que l'utilisateur continue de taper sa recherche
  handleSearchChange = (e, {value}) => {
    this.setState({isLoading: true, value})

    setTimeout(() => {
      if (this.state.value.length < 1) return this.resetComponent()

      const re = new RegExp(_.escapeRegExp(this.state.value), 'i')
      const isMatch = result => re.test(result.name)

      let philosophers = Object.values(this.context.philosophers)
      philosophers.forEach(philosopher => {
        philosopher.title = philosopher.name
        philosopher.image = philosopher.imageURL
      })
      this.setState({
        isLoading: false,
        results: _.filter(philosophers, isMatch),
      })
    }, 300)
  }

  // Cette fonction gère le clic sur un résultat de recherche et appelle la fonction
  // qui permet de récupérer plus de détail à propos du philosophe sélectionné
  handleOnResultSelect = (e, {result}) => {
    let {setCurrentPhilosopher} = this.context
    this.resetComponent()
    setCurrentPhilosopher(result.url)
  }

  // Fonction d'affichage de la barre de menu
  render() {
    const {resetCurrentPhilosopher} = this.context
    const {isLoading, value, results} = this.state
    return (
      <Menu stackable borderless inverted pointing fluid widths={2} size="small" style={{borderRadius: '0px'}}>
        <Menu.Item>
          <Button icon="chevron circle left" onClick={resetCurrentPhilosopher} label="Retour à la liste" />
        </Menu.Item>
        <Menu.Item>
          <Search
            size="large"
            loading={isLoading}
            onResultSelect={this.handleOnResultSelect}
            onSearchChange={_.debounce(this.handleSearchChange, 500, {leading: true})}
            results={results}
            value={value}
          />
        </Menu.Item>
      </Menu>
    )
  }
}

SearchBar.contextType = AppContext
