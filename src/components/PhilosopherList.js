import React from 'react'
import {List, Image, Segment} from 'semantic-ui-react'


// Ce composant prend en paramètre une liste de philosophes 
// et les affiche sous forme de liste dans laquelle on peut défiler
// sans faire défiler toute la page
export default class PhilosopherList extends React.Component {
  render() {
    const {philosophers, setCurrentPhilosopher} = this.props
    return (
      <Segment style={{overflowY: 'scroll'}}>
        <List divided selection animated verticalAlign="middle" size="huge">
          {philosophers && Object.keys(philosophers).length > 0 ? (
            Object.values(philosophers).map(philosopher => {
              return (
                <List.Item key={philosopher.name} onClick={() => setCurrentPhilosopher(philosopher.url)}>
                  {philosopher.imageURL ? (
                    <Image avatar src={philosopher.imageURL} />
                  ) : (
                    <Image avatar src={`https://ui-avatars.com/api/?name=${philosopher.name.split(' ').join('+')}`} />
                  )}
                  <List.Content>{philosopher.name}</List.Content>
                </List.Item>
              )
            })
          ) : (
            <List.Item>No philosophers</List.Item>
          )}
        </List>
      </Segment>
    )
  }
}
