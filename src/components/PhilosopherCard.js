import React from 'react'
import {Card, Image} from 'semantic-ui-react'

// Ce composant permet d'afficher les informations détaillées d'un philosopher
// sous forme de carte dans laquelle on peut faire défiler la description
export default class PhilosopherCard extends React.Component {
  render() {
    const {philosopher} = this.props
    return (
      <Card style={{overflowY: 'hidden', width: '30vw'}}>
        <Image src={`${philosopher.imageURL}`} rounded centered />
        <Card.Content style={{overflowY: 'scroll'}}>
          <Card.Header>{`${philosopher.name}`}</Card.Header>
          <Card.Meta>
            <span>{`${philosopher.birthDate} / ${philosopher.deathDate}`}</span>
          </Card.Meta>
          <Card.Description>{`${philosopher.description}`}</Card.Description>
        </Card.Content>
      </Card>
    )
  }
}
